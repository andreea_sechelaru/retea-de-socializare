package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.MessageService;
import socialnetwork.service.Service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MessageController {

    Utilizator logged;
    Service service;
    MessageService messageService;
    Stage stage;

    //Table - FRIENDS
    private ObservableList<Utilizator> model_friends = FXCollections.observableArrayList();
    @FXML
    private TableView<Utilizator> tableView_friends;
    @FXML
    private TableColumn<Utilizator, String> tableColumnID_friend;
    @FXML
    private TableColumn<Utilizator, String> tableColumnFirstName_friend;
    @FXML
    private TableColumn<Utilizator, String> tableColumnLastName_friend;

    //Table - TO
    private ObservableList<Utilizator> model_to = FXCollections.observableArrayList();
    @FXML
    private TableView<Utilizator> tableView_To;
    @FXML
    private TableColumn<Utilizator, String> tableColumnID_to;
    @FXML
    private TableColumn<Utilizator, String> tableColumnFirstName_to;
    @FXML
    private TableColumn<Utilizator, String> tableColumnLastName_to;

    //Table - MESSAGES
    private ObservableList<Message> model_messages = FXCollections.observableArrayList();
    @FXML
    private TableView<Message> tableView_messages;
    @FXML
    private TableColumn<Message, String> tableColumnFrom;
    @FXML
    private TableColumn<Message, String> tableColumnTo;
    @FXML
    private TableColumn<Message, String> tableColumnMessage;
    @FXML
    private TableColumn<Message, LocalDateTime> tableColumnDate;

    //Text Area - MESSAGE
    @FXML
    private TextArea txtMessage;

    //REPLY
    @FXML
    private CheckBox replyAllCheckBox;
    @FXML
    private CheckBox replyOneCheckBox;




    public void setService(Utilizator logged, Service service, MessageService messageService, Stage stage) {
        this.logged=logged;
        this.service=service;
        this.messageService=messageService;
        this.stage = stage;
        initModel_friends();
        initModel_messages();

    }

    @FXML
    public void initialize() {
        //Table-FRIENDS
        tableColumnID_friend.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("IdString"));
        tableColumnFirstName_friend.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        tableColumnLastName_friend.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));
        tableView_friends.setItems(model_friends);

        //Table-TO
        tableColumnID_to.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("IdString"));
        tableColumnFirstName_to.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        tableColumnLastName_to.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));
        tableView_To.setItems(model_to);

        //Table-MESSAGES
        tableColumnFrom.setCellValueFactory(new PropertyValueFactory<Message, String>("StringFrom"));
        tableColumnTo.setCellValueFactory(new PropertyValueFactory<Message, String>("StringTo"));
        tableColumnMessage.setCellValueFactory(new PropertyValueFactory<Message, String>("message"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<Message, LocalDateTime>("data"));
        tableView_messages.setItems(model_messages);
    }

    //Table - FRIENDS
    private void initModel_friends() {
        List<Utilizator> list = StreamSupport.stream(service.findfriends(logged).spliterator(), false)
                .collect(Collectors.toList());
        model_friends.setAll(list);
    }

    //Table - MESSAGES
    private void initModel_messages() {
        List<Message> list = StreamSupport.stream(messageService.getMessages(logged).spliterator(), false)
                .collect(Collectors.toList());
        model_messages.setAll(list);
    }



    public void handleAddTo(ActionEvent actionEvent) {
        try{
            Utilizator user = tableView_friends.getSelectionModel().getSelectedItem();
            if(user == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Error");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Select a friend!")));
                alert.showAndWait();
            }
            else{

                String fName = user.getFirstName();
                String lName = user.getLastName();
                model_to.add(user);
                model_friends.remove(user);

            }
        }
        catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea(e.getMessage())));

            alert.showAndWait();
        }
    }

    public void handleRemoveTo(ActionEvent actionEvent) {
        try{
            Utilizator user = tableView_To.getSelectionModel().getSelectedItem();
            if(user == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Error");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Select a friend!")));
                alert.showAndWait();
            }
            else{

                String fName = user.getFirstName();
                String lName = user.getLastName();
                model_friends.add(user);
                model_to.remove(user);

            }
        }
        catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea(e.getMessage())));

            alert.showAndWait();
        }
    }

    public void handleSend(ActionEvent actionEvent) {
        List<Long> toId = new ArrayList<>();
        model_to.stream().map(u->u.getId()).collect(Collectors.toList())
                .forEach(toId::add);
        try {
            Message m = messageService.messageToOne(logged.getId(), toId, txtMessage.getText());
            model_messages.clear();
            initModel_messages();
            if (m != null) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Information");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Message sent!")));

                alert.showAndWait();
            }
        } catch (ValidationException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea(e.getMessage())));

            alert.showAndWait();
        }
    }

    public void replyOne(Long message_id) throws IOException {
        try{
            Stage newStage = new Stage();
            FXMLLoader ReplyLoader = new FXMLLoader();
            ReplyLoader.setLocation(getClass().getResource("/views/userReplyOneView.fxml"));
            BorderPane LogInLayout = ReplyLoader.load();
            ReplyOneController replyController = ReplyLoader.getController();
            Scene messageScene = new Scene(LogInLayout);
            newStage.setScene(messageScene);
            replyController.setService(messageService, newStage, message_id, txtMessage.getText(), logged.getId());

            newStage.show();
            newStage.setWidth(350);
        }
        catch (ValidationException e)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea(e.getMessage())));

            alert.showAndWait();
        }

    }

    public void replyAll(Long id)
    {
        try{
            Message m = messageService.replyToAll(id, logged.getId(), txtMessage.getText());
            if(m != null) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Information");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Reply sent!")));

                alert.showAndWait();
            }
        }
        catch (ValidationException e)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea(e.getMessage())));

            alert.showAndWait();
        }
    }

    public void handleReply(ActionEvent actionEvent) {
        if(replyOneCheckBox.isSelected() && replyAllCheckBox.isSelected() || !replyOneCheckBox.isSelected() && !replyAllCheckBox.isSelected())
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("You have to select one CheckBox!")));
            alert.showAndWait();
        }
        try {
            Message m = tableView_messages.getSelectionModel().getSelectedItem();
            if(m == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Error");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Select the message!")));

                alert.showAndWait();
            }
            else {
                if (replyOneCheckBox.isSelected()) {
                    replyOne(m.getId());
                } else
                    replyAll(m.getId());
            }
        }
        catch (ValidationException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea(e.getMessage())));

            alert.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void handleRefresh(ActionEvent actionEvent) {
        initModel_messages();
        initModel_friends();
        model_to.clear();
    }
}
