package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import socialnetwork.domain.Request;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.MessageService;
import socialnetwork.service.RequestService;
import socialnetwork.service.Service;

import java.io.IOException;

public class LoginController {

    Stage currentStage;
    Stage primaryStage;
    Service srv;
    RequestService requestService;
    MessageService messageService;

    @FXML
    TextField txtFirstName;
    @FXML
    TextField txtLastName;

    public void setPrimaryStage(Stage primaryStage, Service srv, RequestService requestService, MessageService messageService) {
        this.primaryStage=primaryStage;
        this.currentStage=primaryStage;
        this.srv=srv;
        this.requestService = requestService;
        this.messageService = messageService;
    }


    public void handleLogIn() throws  Exception
    {
        //validare camp nenul
        if(txtFirstName.getText().length() == 0|| txtLastName.getText().length() == 0){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("ERROR!!");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("First Name or Last Name can't be null!")));

            alert.showAndWait();
        }
        else{
            //cautare utilizator in memorie
            int ok = 0;
            for(Utilizator u : srv.getAll()){
                if(u.getFirstName().equals(txtFirstName.getText()) && u.getLastName().equals(txtLastName.getText())){
                    ok=1;
                    break;
                }
            }
            //eroare daca nu exista utilizatorul
            if(ok == 0){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("ERROR!!");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("This user doesen't exist!")));

                alert.showAndWait();
            }
            else{ //utilizator existent
                String firstName = txtFirstName.getText();
                String lastName = txtLastName.getText();

                Stage newStage = new Stage();
                //main menu
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/mainMenuView.fxml"));
                BorderPane rootLayout = loader.load();
                newStage.setScene(new Scene(rootLayout));

                MainMenuController controller = loader.getController();
                controller.setPrimaryStage(primaryStage, newStage, firstName, lastName, srv);

                //FriiendshipsUsersView
                FXMLLoader userLoader = new FXMLLoader();
                userLoader.setLocation(getClass().getResource("/views/friendshipsUsersView.fxml"));
                AnchorPane messageTaskLayout = userLoader.load();
                FriendshipsUsersController friendshipsUsersController = userLoader.getController();
                friendshipsUsersController.setService(srv, requestService, messageService, firstName, lastName);
                controller.setCenterUsersLayout(messageTaskLayout);
                rootLayout.setCenter(messageTaskLayout);

                newStage.show();
                newStage.setWidth(1020);
                newStage.setHeight(700);

            }

        }

    }




}
