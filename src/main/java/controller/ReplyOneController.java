package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.MessageService;
import socialnetwork.service.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ReplyOneController {

    MessageService messageService;
    Stage stage;
    Long message_id;
    String message;
    Long logged;

    //Reply ONE
    @FXML
    private ObservableList<Utilizator> model_replyOne = FXCollections.observableArrayList();
    @FXML
    private TableView<Utilizator> tableView_replyOne;
    @FXML
    private TableColumn<Utilizator, String> tableColumnID_replyOne;
    @FXML
    private TableColumn<Utilizator, String> tableColumnFirstName_replyOne;
    @FXML
    private TableColumn<Utilizator, String> tableColumnLastName_replyOne;

    //Table - REPLY ONE
    private void initModel_replyOne(Long message_id, Long logged) {
        List<Utilizator> list = StreamSupport.stream(messageService.getRepliers(message_id, logged).spliterator(), false)
                .collect(Collectors.toList());
        model_replyOne.setAll(list);
    }

    @FXML
    public void initialize() {
        //Table-TO
        tableColumnID_replyOne.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("IdString"));
        tableColumnFirstName_replyOne.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        tableColumnLastName_replyOne.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));
        tableView_replyOne.setItems(model_replyOne);
    }

    public void setService(MessageService messageService, Stage stage, Long message_id, String message, Long logged) {
        this.messageService=messageService;
        this.stage = stage;
        this.message_id = message_id;
        this.message = message;
        this.logged = logged;
        initModel_replyOne(message_id, logged);
    }

    public void handleSendReply(ActionEvent actionEvent) {
        try {
            Utilizator u = tableView_replyOne.getSelectionModel().getSelectedItem();
            if (u == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Error");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Select one user!")));
                alert.showAndWait();
            } else {
                Message m = messageService.replyToOne(message_id, logged, u.getId(), message);
                if (m != null) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText("Information");
                    alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Reply sent!")));

                    alert.showAndWait();
                    stage.close();
                }
            }
        }
        catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea(e.getMessage())));

            alert.showAndWait();
        }


    }
}
