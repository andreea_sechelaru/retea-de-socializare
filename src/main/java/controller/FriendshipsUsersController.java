package controller;

import events.FriendshipsUsersChangeEvent;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import observer.Observer;
import socialnetwork.domain.Request;
import socialnetwork.domain.TypeRequest;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.MessageService;
import socialnetwork.service.RequestService;
import socialnetwork.service.Service;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FriendshipsUsersController implements Observer<FriendshipsUsersChangeEvent> {


    private Service service;
    private MessageService messageService;
    private RequestService requestService;
    private ObservableList<Utilizator> model_friends = FXCollections.observableArrayList();
    private ObservableList<Utilizator> model_users = FXCollections.observableArrayList();
    private ObservableList<Request> model_request = FXCollections.observableArrayList();

    //Logged user info
    private String FirstNameLoggedUser;
    private String LastNameLoggedUser;

    //First table - FRIENDS
    @FXML
    private TableView<Utilizator> tableView_friends;
    @FXML
    private TableColumn<Utilizator, String> tableColumnFirstName_friend;
    @FXML
    private TableColumn<Utilizator, String> tableColumnLastName_friend;
    @FXML
    TextField txtFirstName_friend;
    @FXML
    TextField txtLastName_friend;
    //Second table - USERS
    @FXML
    private TableView<Utilizator> tableView_users;
    @FXML
    private TableColumn<Utilizator, String> tableColumnFirstName_user;
    @FXML
    private TableColumn<Utilizator, String> tableColumnLastName_user;

    @FXML
    private TableView<Request> tableView_requests;
    @FXML
    private TableColumn<Request, Long> tableColumnFrom;
    @FXML
    private TableColumn<Request, Long> tableColumnTo;
    @FXML
    private TableColumn<Request, TypeRequest> tableColumnStatus;
    @FXML
    private TableColumn<Request, LocalDateTime> tableColumnDate;



    @FXML
    TextField txtFirstName_user;
    @FXML
    TextField txtLastName_user;

    @FXML
    javafx.scene.control.TextArea taskShowFriendRequests;

    @FXML
    javafx.scene.control.Label labelStatus;



    //First table - FRIENDS
    public void handleFilter() {
        String firstName = txtFirstName_friend.getText();
        String lastName = txtLastName_friend.getText();

        Predicate<Utilizator> firstNamePredicate = u -> u.getFirstName().contains(firstName);
        Predicate<Utilizator> lastNamePredicate = u -> u.getLastName().contains(lastName);

        model_friends.setAll(StreamSupport.stream(service.getAll().spliterator(), false)
                .filter(firstNamePredicate.and(lastNamePredicate))
                .collect(Collectors.toList()));
    }

    public void handleFilterFirstName(KeyEvent keyEvent) {
        handleFilter();
    }

    public void handleFilterLastName(KeyEvent keyEvent) {
        handleFilter();
    }



    private void initModel_friends() {
        List<Utilizator>list = StreamSupport.stream(service.findfriends(FirstNameLoggedUser, LastNameLoggedUser).spliterator(), false)
                .collect(Collectors.toList());
        model_friends.setAll(list);
    }

    //Second table - USERS
    public void handleFilter2() {
        String firstName2 = txtFirstName_user.getText();
        String lastName2 = txtLastName_user.getText();

        Predicate<Utilizator> firstNamePredicate2 = u -> u.getFirstName().contains(firstName2);
        Predicate<Utilizator> lastNamePredicate2 = u -> u.getLastName().contains(lastName2);

        model_users.setAll(StreamSupport.stream(service.getAll().spliterator(), false)
                .filter(firstNamePredicate2.and(lastNamePredicate2))
                .collect(Collectors.toList()));
    }

    public void handleFilterFirstName2(KeyEvent keyEvent) {
        handleFilter2();
    }

    public void handleFilterLastName2(KeyEvent keyEvent) {
        handleFilter2();
    }

    private void initModel_users() {
        List<Utilizator>list = StreamSupport.stream(service.findunfriends(FirstNameLoggedUser, LastNameLoggedUser).spliterator(), false)
                .collect(Collectors.toList());
        model_users.setAll(list);
    }

    //Common functions - INITIALIZE
    @FXML
    public void initialize() {
        //FIRST TABLE
        tableColumnFirstName_friend.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        tableColumnLastName_friend.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));
        tableView_friends.setItems(model_friends);

        //SECOND TABLE
        tableColumnFirstName_user.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        tableColumnLastName_user.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));
        tableView_users.setItems(model_users);

        //THIRD TABLE
        tableColumnFrom.setCellValueFactory(new PropertyValueFactory<Request, Long>("from"));
        tableColumnTo.setCellValueFactory(new PropertyValueFactory<Request, Long>("to"));
        tableColumnStatus.setCellValueFactory(new PropertyValueFactory<Request, TypeRequest>("status"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<Request, LocalDateTime>("date"));

        tableView_requests.setItems(model_request);

    }

    @Override
    public void update(FriendshipsUsersChangeEvent friendshipsUsersChangeEvent) {
        model_friends.setAll(StreamSupport.stream(service.getAll().spliterator(), false)
                .collect(Collectors.toList()));
    }

    public void setService(Service service, RequestService requestService, MessageService messageService, String FirstName, String LastName) {
        this.service = service;
        this.requestService = requestService;
        this.messageService = messageService;
        service.addObserver(this);
        requestService.addObserver(this);
        messageService.addObserver(this);
        //init logged user info
        FirstNameLoggedUser = FirstName;
        LastNameLoggedUser = LastName;
        initModel_friends();
        initModel_users();

    }

    private Long getIdLoggedUser(){
        Long idLogged = 0L;
        for (Utilizator u : service.getAll()){
            if(u.getFirstName().equals(FirstNameLoggedUser) && u.getLastName().equals(LastNameLoggedUser)){
                idLogged = u.getId();
            }
        }
        return idLogged;
    }

    private Long getIdFromName(String fName, String lName){
        Long id=0L;
        for (Utilizator u : service.getAll()){
            if(u.getFirstName().equals(fName) && u.getLastName().equals(lName)){
                id = u.getId();
            }
        }
        return id;
    }

    private Utilizator getLoggedUser(){
        Long idLogged = getIdLoggedUser();
        return service.gaseste(idLogged);
    }

//    private Long getIdRequest(Long from, Long to){
//        Long id=0L;
//        for (Request u : requestService.getAll()){
//            if(u.getFrom() && u.getLastName().equals(lName)){
//                id = u.getId();
//            }
//        }
//        return id;
//    }

    @FXML private javafx.scene.control.Button buttonShowFriendRequests;
    @FXML private javafx.scene.control.Button buttonShowMyFriendRequests;
    private Task<List<Request>> taskRequests;

    public Task<List<Request>> createRequests(){
        return new Task<List<Request>>() {
            @Override
            protected List<Request> call() throws Exception {
                List<Request> requests = model_request.stream().collect(Collectors.toList());
                int index;
                List<Request> result=new ArrayList<>();
                for (index = 0; index < requests.size(); index++) {
                    if (this.isCancelled() == true) {
                        return null;
                    } else {
                        Request m = requests.get(index);
                        //m.execute();
                        result.add(m);
                        updateMessage("Request -- From -- " + m.getFrom() + " -- To -- "+ m.getTo() + " -- Status -- " + m.getStatus());
                        updateProgress(index, requests.size() - 1);
                    }
                }
                return result;
            }

        };
    }

    public void handleShowFriendRequests(ActionEvent actionEvent){
        taskRequests = createRequests();
        labelStatus.textProperty().bind(taskRequests.messageProperty());
        buttonShowFriendRequests.setDisable(true);


        taskRequests.setOnSucceeded(x->{
            labelStatus.textProperty().unbind();
            labelStatus.setText(taskRequests.getValue().size() +" Requests were showed....");
            buttonShowFriendRequests.setDisable(false);

        });

        Thread th=new Thread(taskRequests);
        //th.setDaemon(true);
        th.start();
    }

    public void handleShowFriendRequests(javafx.event.ActionEvent actionEvent) {
        buttonShowFriendRequests.setDisable(true);
        initModel_request();
        buttonShowFriendRequests.setDisable(false);
    }

    private void initModel_request() {
        List<Request>list = StreamSupport.stream(requestService.findrequests(getIdLoggedUser()).spliterator(), false)
                .collect(Collectors.toList());
        model_request.setAll(list);
    }

//    private void initModel_request() {
//
//        //taskShowFriendRequests.clear();
//        Long id = getIdLoggedUser();
//        System.out.println("USER" + id);
//        model_request.clear();
//        taskShowFriendRequests.clear();
//        List<Request>list = StreamSupport.stream(requestService.findrequests(id).spliterator(), false)
//                .collect(Collectors.toList());
//        model_request.setAll(list);
//        for(Request r : list)
//        {
//            String cerere = "Request from: " + r.getFrom() + " to: " + r.getTo() + " status: " + r.getStatus() + " date: " + r.getDate().toLocalDate();
//            taskShowFriendRequests.appendText(cerere+"\n" + "-----------------------------------------" + "\n");
//        }
//    }

    public void handleShowMyFriendRequests(javafx.event.ActionEvent actionEvent) {

        buttonShowMyFriendRequests.setDisable(true);
        initModel_Myrequest();
        buttonShowMyFriendRequests.setDisable(false);
    }

    private void initModel_Myrequest() {
        List<Request>list = StreamSupport.stream(requestService.myFriendRequets(getIdLoggedUser()).spliterator(), false)
                .collect(Collectors.toList());
        model_request.setAll(list);
    }

//    private void initModel_Myrequest() {
//
//        //taskShowFriendRequests.clear();
//        Long id = getIdLoggedUser();
//        System.out.println("USER" + id);
//        model_request.clear();
//        taskShowFriendRequests.clear();
//        List<Request>list = StreamSupport.stream(requestService.myFriendRequets(id).spliterator(), false)
//                .collect(Collectors.toList());
//        model_request.setAll(list);
//        for(Request r : list)
//        {
//            String cerere = "Request from: " + r.getFrom() + " to: " + r.getTo() + " status: " + r.getStatus() + " date: " + r.getDate().toLocalDate();
//            taskShowFriendRequests.appendText(cerere+"\n" + "-----------------------------------------" + "\n");
//        }
//    }



    public void handleAddFriend(javafx.event.ActionEvent actionEvent) {
        //taskShowFriendRequests.setText("Salut");


        try{
            Utilizator user = tableView_users.getSelectionModel().getSelectedItem();
            if(user == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Error");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Select a friend!")));

                alert.showAndWait();
            }
            else{
                String fName = user.getFirstName();
                String lName = user.getLastName();
                Long id = getIdFromName(fName, lName);
                Long idLogged = getIdLoggedUser();
                requestService.sendRequest(idLogged, id);

                model_friends.clear();
                initModel_friends();
                model_users.clear();
                initModel_users();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Information");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Friend request sent!")));

                alert.showAndWait();
            }
        }
        catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea(e.getMessage())));

            alert.showAndWait();
        }

    }

    public void handleRemoveFriend(javafx.event.ActionEvent actionEvent) {
        try{
            Utilizator user = tableView_friends.getSelectionModel().getSelectedItem();
            if(user == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Error");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Select the friend!")));

                alert.showAndWait();
            }
            else{
                String fName = user.getFirstName();
                String lName = user.getLastName();
                Long id = getIdFromName(fName, lName);
                Long idLogged = getIdLoggedUser();
                service.removePrieten(idLogged, id);
                model_friends.clear();
                initModel_friends();
                model_users.clear();
                initModel_users();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Information");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Friend removed!")));

                alert.showAndWait();
            }


        }
        catch (ValidationException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea(e.getMessage())));

            alert.showAndWait();
        }


    }

    public void handleAccept(javafx.event.ActionEvent actionEvent) {
        try{
            Request user = tableView_requests.getSelectionModel().getSelectedItem();
            if(user == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Error");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Select the request!")));

                alert.showAndWait();
            }
            else{
                Long from = user.getFrom();
                Long to = user.getTo();
                requestService.approve(to, from);
                model_friends.clear();
                initModel_friends();
                model_users.clear();
                initModel_users();
                model_request.clear();
                initModel_request();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Information");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Friend added!")));

                alert.showAndWait();
            }


        }
        catch (ValidationException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea(e.getMessage())));

            alert.showAndWait();
        }


    }


    public void handleReject(javafx.event.ActionEvent actionEvent) {
        try{
            Request user = tableView_requests.getSelectionModel().getSelectedItem();
            if(user == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Error");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Select the request!")));

                alert.showAndWait();
            }
            else{
                Long from = user.getFrom();
                Long to = user.getTo();
                requestService.reject(to, from);
                model_friends.clear();
                initModel_friends();
                model_users.clear();
                initModel_users();
                model_request.clear();
                initModel_request();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Information");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Friend removed!")));

                alert.showAndWait();
            }


        }
        catch (ValidationException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea(e.getMessage())));

            alert.showAndWait();
        }


    }

    public void handleCancel(javafx.event.ActionEvent actionEvent) {
        try{
            Request user = tableView_requests.getSelectionModel().getSelectedItem();
            if(user == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Error");
                alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Select the request!")));

                alert.showAndWait();
            }
            else{
                Long from = user.getFrom();
                Long to = user.getTo();
                if(!from.equals(getIdLoggedUser())){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText("Error");
                    alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("This request isn't yours!")));

                    alert.showAndWait();
                }
                else{
                    requestService.cancelRequest(from, to);
                    model_request.clear();
                    initModel_request();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText("Information");
                    alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea("Request canceled!")));

                    alert.showAndWait();
                }
            }
        }
        catch (ValidationException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error");
            alert.getDialogPane().setExpandableContent(new javafx.scene.control.ScrollPane(new javafx.scene.control.TextArea(e.getMessage())));

            alert.showAndWait();
        }


    }


    public void handleMessages(javafx.event.ActionEvent actionEvent) throws IOException {
        Stage newStage = new Stage();
        FXMLLoader MessageLoader = new FXMLLoader();
        MessageLoader.setLocation(getClass().getResource("/views/messageView.fxml"));
        BorderPane LogInLayout = MessageLoader.load();
        MessageController messageController = MessageLoader.getController();
        Scene messageScene = new Scene(LogInLayout);
        newStage.setScene(messageScene);
        messageController.setService(getLoggedUser(), service, messageService, newStage);

        newStage.show();
        newStage.setWidth(850);
    }
}
