package controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.Service;

public class MainMenuController {

    Service service;
    Stage currentStage;
    Stage previousStage;
    AnchorPane users;
    AnchorPane friendships;

    //MainMenu - LOGGED
    @FXML
    TextField FirstNameLogged;
    @FXML
    TextField LastNameLogged;
    @FXML
    TextField IdLogged;

    public void setCenterUsersLayout(AnchorPane usersLayout) {
        this.users=usersLayout;
    }


//
//    @FXML
//    public void handleMessageTaskCRUD()
//    {
//        //this.primaryStage.setScene(messageTaskScene);
//        BorderPane rootLayout= (BorderPane) this.primaryStage.getScene().getRoot();
//        rootLayout.setCenter(messages);
//
//    }
//
//    @FXML
//    public void handleGradeCRUD()
//    {
//        BorderPane rootLayout= (BorderPane) this.primaryStage.getScene().getRoot();
//        rootLayout.setCenter(grades);
//
//    }
//
//    public void setCenterMessageLayout(AnchorPane messageTaskLayout) {
//        this.messages=messageTaskLayout;
//    }
//
//    public void setCenterGradeLayout(AnchorPane gradeLayout) {
//        this.grades=gradeLayout;
//    }

    public void handleLogOut()
    {
        currentStage.close();
        previousStage.show();

    }
    public void setPrimaryStage(Stage previousStage, Stage currentStage, String FirstName, String LastName, Service service) {
        this.service=service;
        this.currentStage=currentStage;
        this.previousStage = previousStage;
        FirstNameLogged.setText(FirstName);
        LastNameLogged.setText(LastName);
        IdLogged.setText(getIdLoggedUser().toString());
    }

    private Long getIdLoggedUser(){

        Long idLogged = 0L;
        for (Utilizator u : service.getAll()){
            if(u.getFirstName().equals(FirstNameLogged.getText()) && u.getLastName().equals(LastNameLogged.getText())){
                idLogged = u.getId();
            }
        }
        return idLogged;
    }
}
