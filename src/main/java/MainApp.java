import controller.FriendshipsUsersController;
import controller.LoginController;
import controller.MainMenuController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.FriendshipValidator;
import socialnetwork.domain.validators.MessageValidator;
import socialnetwork.domain.validators.RequestValidator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.FriendshipFile;
import socialnetwork.repository.file.MessageFile;
import socialnetwork.repository.file.RequestFile;
import socialnetwork.repository.file.UtilizatorFile;
import socialnetwork.service.MessageService;
import socialnetwork.service.RequestService;
import socialnetwork.service.Service;

public class MainApp extends Application {
    Repository<Long, Utilizator> userFileRepository;
    Repository<Tuple<Long, Long>, Prietenie>  friendshipRepository;
    Repository<Long, Message> messageRepository;
    Repository<Long, Request> requestRepository;
    Service srv;
    MessageService message_srv;
    RequestService request_srv;

    public static void main(String[] args){launch(args);}

    @Override
    public void start(Stage primaryStage) throws Exception {
        //repository
        userFileRepository = new UtilizatorFile("data/users.csv", new UtilizatorValidator());
        friendshipRepository = new FriendshipFile("data/friendships.csv", new FriendshipValidator());
        messageRepository = new MessageFile("data/messages.csv", new MessageValidator());
        requestRepository = new RequestFile("data/request.csv", new RequestValidator());
        //service
        srv = new Service(userFileRepository, friendshipRepository);
        message_srv = new MessageService(userFileRepository, messageRepository);
        request_srv = new RequestService(requestRepository, userFileRepository, friendshipRepository, srv);

        init1(primaryStage);

    }

    private void init1(Stage primaryStage) throws  Exception
    {
        //Log in
        FXMLLoader LogInLoader = new FXMLLoader();
        LogInLoader.setLocation(getClass().getResource("/views/loginView.fxml"));
        BorderPane LogInLayout = LogInLoader.load();
        LoginController loginController = LogInLoader.getController();
        Scene loginScene = new Scene(LogInLayout);
        primaryStage.setScene(loginScene);
        loginController.setPrimaryStage(primaryStage, srv, request_srv, message_srv);

        primaryStage.show();
        primaryStage.setWidth(600);


    }
}
