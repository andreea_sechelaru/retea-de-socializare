package socialnetwork.service;

import events.FriendshipsUsersChangeEvent;
import observer.Observable;
import observer.Observer;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.RequestFile;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class RequestService  implements Observable<FriendshipsUsersChangeEvent> {
    private Repository<Long, Request> requestRepo;
    private Repository<Long, Utilizator> repo;
    private Repository<Tuple<Long, Long>, Prietenie> friendshipRepo;
    private Service srv;

    public RequestService(Repository<Long, Request> requestRepo, Repository<Long, Utilizator> repo, Repository<Tuple<Long, Long>, Prietenie> friendshipRepo, Service srv) {
        this.requestRepo = requestRepo;
        this.friendshipRepo = friendshipRepo;
        this.repo = repo;
        this.srv = srv;

    }


    public Iterable<Request> getAll(){
        return requestRepo.findAll();
    }

    public void sendRequest(Long id_1, Long id_2) {
        int found = 0;
        int status = 0;
        int ok = 1;
        if (repo.findOne(id_1) == null) {
            throw new ValidationException("Invalid the first user!");
        }
        if (repo.findOne(id_2) == null) {
            throw new ValidationException("Invalid the second user!");
        }
        Tuple idPrietenie1 = new Tuple(id_1, id_2);
        Tuple idPrietenie2 = new Tuple(id_2, id_1);
        if (friendshipRepo.findOne(idPrietenie1) == null && friendshipRepo.findOne(idPrietenie2) == null) {
            for (Request curent : requestRepo.findAll()) {
                if (curent.getTo() == id_1 && curent.getFrom() == id_2) {
                    if (curent.getStatus() == TypeRequest.pending) {
                        ok=0;
                    }
                    else{
                        ok=1;
                    }
                }
            }
            if(ok==0){
                throw new ValidationException("You can't send a request to this user! You have to approve or reject his request");
            }
            for (Request curent : requestRepo.findAll()) {
                if (curent.getFrom() == id_1 && curent.getTo() == id_2) {
                    found = 1;
                    if (curent.getStatus() == TypeRequest.rejected || curent.getStatus() == TypeRequest.canceled) {
                        status = 1;
                    } else {
                        status = 0;
                    }
                }
            }
            if (found == 0 || (found == 1 && status == 1)) {
                Long ID = 0L;
                for (Request curent : requestRepo.findAll()) {
                    if (curent.getId() > ID) {
                        ID = curent.getId();
                    }
                }
                ID = ID + 1L;

                Request r = new Request(id_1, id_2);
                r.setId(ID);
                r.setDate(LocalDateTime.now());
                requestRepo.save(r);
            } else {
                if (status == 0) {
                    throw new ValidationException("You can't send another request to this user!");
                }
            }
        } else {
            throw new ValidationException("You and this user are already friends!");
        }

    }

    public void approve(Long id_1, Long id_2) {
        if (repo.findOne(id_1) == null) {
            throw new ValidationException("Invalid the first user!");
        }
        if (repo.findOne(id_2) == null) {
            throw new ValidationException("Invalid the second user!");
        }
        int ok = 0;
        int pending = 0;
        Request curent_request = new Request(id_2, id_1);
        for (Request curent : requestRepo.findAll()) {
            if (curent.getTo() == id_1 && curent.getFrom() == id_2) {
                ok = 1;
                if (curent.getStatus() == TypeRequest.pending) {
                    pending = 1;
                    //curent_request = curent;
                } else {
                    pending = 0;
                }
            }
        }
        if (ok == 0) {
            throw new ValidationException("This request doesn't exist!");
        } else {
            if (pending == 0) {
                throw new ValidationException("You can't approve or reject this request!");
            } else {
                curent_request.setStatus(TypeRequest.approved);
                srv.addPrietenie(id_1, id_2);
                Long ID = 0L;
                for (Request curent : requestRepo.findAll()) {
                    if (curent.getId() > ID) {
                        ID = curent.getId();
                    }
                }
                ID = ID + 1L;
                curent_request.setId(ID);
                curent_request.setDate(LocalDateTime.now());
                requestRepo.save(curent_request);
            }
        }
    }

    public void reject(Long id_1, Long id_2) {
        if (repo.findOne(id_1) == null) {
            throw new ValidationException("Invalid the first user!");
        }
        if (repo.findOne(id_2) == null) {
            throw new ValidationException("Invalid the second user!");
        }
        int ok = 0;
        int pending = 0;
        Request curent_request = new Request(id_2, id_1);
        for (Request curent : requestRepo.findAll()) {
            if (curent.getTo() == id_1 && curent.getFrom() == id_2) {
                ok = 1;
                if (curent.getStatus() == TypeRequest.pending) {
                    pending = 1;
                    //curent_request = curent;
                } else {
                    pending = 0;
                }
            }
        }
        System.out.println(pending);
        if (ok == 0) {
            throw new ValidationException("This request doesn't exist!");
        } else {
            if (pending == 0) {
                throw new ValidationException("You can't approve or reject this request!");
            } else {
                curent_request.setStatus(TypeRequest.rejected);
                Long ID = 0L;
                for (Request curent : requestRepo.findAll()) {
                    if (curent.getId() > ID) {
                        ID = curent.getId();
                    }
                }
                ID = ID + 1L;
                curent_request.setId(ID);
                curent_request.setDate(LocalDateTime.now());
                requestRepo.save(curent_request);
            }
        }
    }

    public List<Request> showFriendRequests(Long id){
        if (repo.findOne(id) == null) {
            throw new ValidationException("Invalid the first user!");
        }
        List<Request> requests = new ArrayList<Request>();
        for(Request request : requestRepo.findAll()){
            if(request.getTo() == id){
                requests.add(request);
            }
        }
        return requests;
    }

    public List<Request> findrequests(Long id) {
        List<Request> results = new ArrayList<>();
        Predicate<Request> fromPredicate = r -> r.getTo() == id;
        for(Request request : requestRepo.findAll()){
            if(request.getTo() ==id){
                results.add(request);
            }
        }
        return results;
    }

    public List<Request> myFriendRequets(Long id){
        List<Request> results = new ArrayList<>();
        int ok = 0;
        for(Request r : requestRepo.findAll()){
            if(r.getFrom() == id){
                results.add(r);
            }
        }
        return results;
    }

    public void cancelRequest(Long from, Long to){
        if (repo.findOne(from) == null) {
            throw new ValidationException("Invalid the first user!");
        }
        if (repo.findOne(to) == null) {
            throw new ValidationException("Invalid the second user!");
        }
        int ok = 0;
        for(Request r : requestRepo.findAll()){
            if(r.getFrom() == from && r.getTo() == to){
                if(r.getStatus() == TypeRequest.pending) {
                    ok = 1;
                }
                else
                    ok = 0;
            }
        }
        if(ok == 1){
            Request request = new Request(from, to);
            request.setStatus(TypeRequest.canceled);
            request.setDate(LocalDateTime.now());
            Long ID = 0L;
            for (Request curent : requestRepo.findAll()) {
                if (curent.getId() > ID) {
                    ID = curent.getId();
                }
            }
            ID = ID + 1L;
            request.setId(ID);
            requestRepo.save(request);

        }
        else{
            throw new ValidationException("This request can't be canceled!");
        }
    }


    private List<Observer<FriendshipsUsersChangeEvent>> observers=new ArrayList<>();

    @Override
    public void addObserver(Observer<FriendshipsUsersChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<FriendshipsUsersChangeEvent> e) {
        //observers.remove(e);
    }

    @Override
    public void notifyObservers(FriendshipsUsersChangeEvent t) {
        observers.stream().forEach(x->x.update(t));
    }
}

