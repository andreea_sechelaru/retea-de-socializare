package socialnetwork.service;

//import jdk.incubator.foreign.ValueLayout;
import events.FriendshipsUsersChangeEvent;
import observer.Observable;
import observer.Observer;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.RequestFile;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Service implements Observable<FriendshipsUsersChangeEvent> {
    private Repository<Long, Utilizator> repo;
    private Repository<Tuple<Long, Long>, Prietenie> friendshipRepo;


    public Service(Repository<Long, Utilizator> repo, Repository<Tuple<Long, Long>, Prietenie> friendshipRepo) {
        this.repo = repo;
        this.friendshipRepo = friendshipRepo;
        loadAllData();
    }


    private List<Observer<FriendshipsUsersChangeEvent>> observers=new ArrayList<>();

    @Override
    public void addObserver(Observer<FriendshipsUsersChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<FriendshipsUsersChangeEvent> e) {
        //observers.remove(e);
    }

    @Override
    public void notifyObservers(FriendshipsUsersChangeEvent t) {
        observers.stream().forEach(x->x.update(t));
    }



    /**
     * Adauga un utilizator
     * @param u - Utilizator
     * @return - Utilizator sau null daca exista deja unul cu id-ul dat
     */
    public Utilizator addUtilizator(Utilizator u) {
        Utilizator task = repo.save(u);
        return task;
    }



    /**
     * Sterge un utilizator
     * @param ID - Long
     * @return = Utilizator sau arunca exceptie daca nu exista id-ul dat
     */
    public Utilizator removeUtilizator(Long ID) {
        if(repo.findOne(ID) == null)
        {
            throw new ValidationException("Invalid users!");
        }
        Utilizator u1 = repo.findOne(ID);

            Iterable<Utilizator> friends = new ArrayList<Utilizator>(u1.getFriends());
            for (Utilizator friend : friends) {
                //System.out.println("step");
                Prietenie p = removePrieten(ID, friend.getId());

            }
            return repo.delete(ID);

    }

    /**
     * Adauga prietenie intre doi utilizatori
     * @param ID1 - Long
     * @param ID2 - Long
     * @return - Prietenie sau exceptie daca nu exista id-urile sau daca exista deja prietenia
     */
    public Prietenie addPrietenie(Long ID1, Long ID2) {

        if(repo.findOne(ID1) == null || repo.findOne(ID2) == null)
        {
            throw new ValidationException("Invalid users!");
        }
        Utilizator u1 = repo.findOne(ID1);
        Utilizator u2 = repo.findOne(ID2);
        Tuple idPrietenie = new Tuple(u1.getId(), u2.getId());
        Prietenie p = new Prietenie(LocalDateTime.now());
        p.setId(idPrietenie);
        u1.addPrieten(u2);
        u2.addPrieten(u1);
        friendshipRepo.save(p);

        return p;
    }

    /**
     * Sterge prietenie dintre doi utilizatori
     * @param ID1 - Long
     * @param ID2 - LOng
     * @return - Prietenie sau arunca exceptii daca nu exista prietenia sau id-urile date
     */
    public Prietenie removePrieten(Long ID1, Long ID2) {

        if(ID1 == null || ID2 == null){
            throw new ValidationException("The id can't be null");
        }
        Tuple friendshipID = new Tuple(ID1, ID2);
        //friendshipRepo.delete(friendshipID);
        if(friendshipRepo.findOne(friendshipID) != null)
        {
            Prietenie p1 = friendshipRepo.delete(friendshipID);
            if( p1 != null)
            {
                repo.findOne(ID1).removePrieten(repo.findOne(ID2));
                repo.findOne(ID2).removePrieten(repo.findOne(ID1));
                return p1;
            }
        }
        else{
            Tuple friendshipID2 = new Tuple(ID2, ID1);
            if(friendshipRepo.findOne(friendshipID2) != null)
            {

                Prietenie p2 = friendshipRepo.delete(friendshipID2);
                if( p2 != null)
                {
                    repo.findOne(ID1).removePrieten(repo.findOne(ID2));
                    repo.findOne(ID2).removePrieten(repo.findOne(ID1));
                    return p2;
                }
            }
        }

        return null;
    }

    public List<Prietenie> getPrietenii()
    {

        List<Prietenie> friends = new ArrayList<>();
        for(Prietenie p :  friendshipRepo.findAll())
            friends.add(p);
        return friends;
    }

    private void loadAllData()
    {
        friendshipRepo.findAll().forEach(this::loadFriendship);
    }
    public void loadFriendship(Prietenie e){
        Tuple<Long, Long> id = e.getId();
        Utilizator u1 = repo.findOne(id.getLeft());
        Utilizator u2 = repo.findOne(id.getRight());
        u1.addPrieten(u2);
        u2.addPrieten(u1);


    }

    public Utilizator gaseste(Long ID)
    {
        if(repo.findOne(ID) == null)
        {
            throw new ValidationException("Invalid user!");
        }
        return repo.findOne(ID);
    }

    /**
     * Numarul de comunitati din retea
     * @return - Integer
     */
    public int componenteConexe()
    {
        Retea retea = Retea.getInstanceRetea();
        return retea.NrComponenteConexe(repo.findAll());
    }

    public List<Long> comunitateSociabila()
    {
        Retea retea = Retea.getInstanceRetea();
        return retea.comunitate(repo.findAll());
    }

    /**
     * Lista utilizatorilor
     * @return - Iterable Utilizator
     */
    public Iterable<Utilizator> getAll(){
        return repo.findAll();
    }


    public void dfs(Utilizator x,Map<Long,Long> vizitat,Map<Long,Long>  distanta,Long time) {
        time++;
        distanta.replace(x.getId(),time);
        vizitat.replace(x.getId(), (long) 1);
        for(Utilizator ele:x.getFriends()) {
            if(vizitat.get(ele.getId()) == 0) {
                Utilizator user = repo.findOne(ele.getId());
                dfs(user,vizitat,distanta,time);
            }
        }
    }

    public String cel_mai_lung_drum(){
        Map<Long,Long>  distanta = new HashMap<Long,Long>();
        Map<Long,Long>  vizitat = new HashMap<Long,Long>();
        String friends = "";
        //Iterable<Utilizator> prieteni =
        for(Utilizator ele:repo.findAll())
        {
            distanta.put(ele.getId(), (long) 0);
            vizitat.put(ele.getId(), (long) 0);
        }
        Integer max=0;
        Utilizator user1 = null;
        for(Utilizator ele:repo.findAll()) {
            dfs(ele,vizitat,distanta, (long) 0);
            for(Utilizator user:repo.findAll()) {
                Long id = user.getId();
                if(distanta.get(id)>max) {
                    max= Math.toIntExact(distanta.get(id));
                    user1 = repo.findOne(id);
                }
                vizitat.replace(id, (long) 0);
                distanta.replace(id, (long) 0);
            }

        }
        dfs(user1,vizitat,distanta, (long) 0);

        System.out.println("distanta: " + distanta);
        for(Utilizator u: repo.findAll())
        {
            Long id = u.getId();
            if(distanta.get(id) > 0){
                friends+=repo.findOne(id).getId()+" "
                        +repo.findOne(id).getFirstName()+" "
                        +repo.findOne(id).getLastName()+"\n";
            }
        }
        return friends;


    }

    public Utilizator findByName(String firstName, String lastName)
    {
        for(Utilizator u : repo.findAll())
        {
            if(u.getFirstName().equals(firstName) && u.getLastName().equals(lastName))
                return u;
        }
        return null;
    }

    public List<Utilizator> findfriends(String firstName, String lastName)
    {
        Utilizator user = findByName(firstName, lastName);
        List<Prietenie> friends = findfriendships(user);
        List<Utilizator> users = new ArrayList<>();
        for(Prietenie friend : friends)
        {
            Long id_left = friend.getId().getLeft();
            Long id_right = friend.getId().getRight();
            if(id_left == user.getId())
                users.add(repo.findOne(id_right));
            else
                users.add(repo.findOne(id_left));
        }
        return users;
    }

    public List<Utilizator> findfriends(Utilizator user)
    {

        List<Prietenie> friends = findfriendships(user);
        List<Utilizator> users = new ArrayList<>();
        for(Prietenie friend : friends)
        {
            Long id_left = friend.getId().getLeft();
            Long id_right = friend.getId().getRight();
            if(id_left == user.getId())
                users.add(repo.findOne(id_right));
            else
                users.add(repo.findOne(id_left));
        }
        return users;
    }

    public List<Prietenie> findfriendships(Utilizator user)
    {
        List<Prietenie> friends = this.getPrietenii();
        Predicate<Prietenie> leftPredicate = f -> f.getId().getLeft() == user.getId();
        Predicate<Prietenie> rightPredicate = f -> f.getId().getRight() == user.getId();
        return friends.stream()
                .filter(leftPredicate.or(rightPredicate))
                .collect(Collectors.toList());
    }

    public List<Utilizator> findunfriends(String firstName, String lastName)
    {
        Utilizator user = findByName(firstName, lastName);
        List<Utilizator> unfriends = new ArrayList<>();
        for(Utilizator u : repo.findAll())
        {
            Tuple friendshipID = new Tuple(user.getId(), u.getId());
            Tuple friendshipID2 = new Tuple(u.getId(), user.getId());
            if(friendshipRepo.findOne(friendshipID)==null && friendshipRepo.findOne(friendshipID2)==null) //nu exista prietenie intre ei
            {
                unfriends.add(u);
            }
        }
        return unfriends;
    }



}
