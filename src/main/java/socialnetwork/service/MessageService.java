package socialnetwork.service;

import events.Event;
import observer.Observable;
import observer.Observer;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MessageService implements Observable {
    private Repository<Long, Utilizator> repo;
    private Repository<Long, Message> messageRepository;

    public MessageService(Repository<Long, Utilizator> repo, Repository<Long, Message> messageRepository) {
        this.repo = repo;
        this.messageRepository = messageRepository;
    }

    public Message messageToOne(Long id, List<Long> TO, String message) {
        if(repo.findOne(id) == null)
        {
            throw new ValidationException("Invalid user FROM!");
        }
        if(message.length()==0)
            throw new ValidationException("Message can't be null!");
        if(TO.isEmpty())
            throw new ValidationException("To list can't be null!");
        Utilizator u1 = repo.findOne(id);
        List<Utilizator> destinatie = new ArrayList<>();
        for(Long id_curent : TO) {
            if(repo.findOne(id_curent) == null)
            {
                throw new ValidationException("Invalid one of the users TO!");
            }
            Utilizator u2 = repo.findOne(id_curent);
            destinatie.add(u2);
        }
        Message m  = new Message(u1, destinatie, message, LocalDateTime.now());

        Long ID = 0L;
        for (Message curent : messageRepository.findAll()){
            if(curent.getId() > ID){
                ID = curent.getId();
            }
        }
        ID = ID + 1L;
        m.setId(ID);
        messageRepository.save(m);
        return m;
    }

    public Message replyToAll(Long id_mess, Long id, String message) {
        if(message.length()==0)
            throw new ValidationException("Message can't be null!");
        if(repo.findOne(id) == null)
        {
            throw new ValidationException("Invalid user FROM!");
        }
        Utilizator uFrom = repo.findOne(id);
        if(messageRepository.findOne(id_mess) == null)
        {
            throw new ValidationException("Invalid ID message!");
        }
        Message m = messageRepository.findOne(id_mess);
        List<Utilizator> TO = new ArrayList<>();
        TO.add(m.getFrom());
        int gasit = 0; //verific daca uFROM exista in lista de TO a mesajului dat
        for(Utilizator u : m.getTo())
        {
            if(!u.getId().equals(uFrom.getId())) //cautam toti carora trebuie sa le trimita mesajul
            {
                TO.add(u);
            }
            else
                gasit = 1;
        }
        if(gasit == 0)
            throw  new ValidationException("User FROM doesn't exist in list to: from this message!");
        Message mesaj_nou = new Message(uFrom, TO, message, LocalDateTime.now());

        Long ID = 0L;
        for (Message curent : messageRepository.findAll()){
            if(curent.getId() > ID){
                ID = curent.getId();
            }
        }
        ID = ID+1L;
        mesaj_nou.setId(ID);

        messageRepository.save(mesaj_nou);
        m.addReplies(mesaj_nou.getId());
        messageRepository.update(m);

        return mesaj_nou;

    }

    public Message replyToOne(Long id_mess, Long id, Long id_user, String message) {

        if(repo.findOne(id) == null)
        {
            throw new ValidationException("Invalid user FROM!");
        }
        if(repo.findOne(id_user) == null)
        {
            throw new ValidationException("Invalid user TO!");
        }
        Utilizator uFrom = repo.findOne(id);
        Utilizator uTO = repo.findOne(id_user);
        if(messageRepository.findOne(id_mess) == null)
        {
            throw new ValidationException("Invalid ID message!");
        }
        Message m = messageRepository.findOne(id_mess);
        List<Utilizator> TO = new ArrayList<>();
        int gasit = 0;
        if(id_user.equals(m.getFrom().getId()))
            TO.add(uTO);
        else{
            for(Utilizator u : m.getTo())
            {
                if (u.getId().equals(id_user)) //verificam daca id ul caruia vrem sa-i raspundem este in lista de to a mesajului dat
                {
                    gasit = 1;
                    break;
                }
            }
            if(gasit == 0)
                throw new ValidationException("User TO doesn't exist in list from:/to: from this message");
            TO.add(uTO);
        }

        Message mesaj_nou = new Message(uFrom, TO, message, LocalDateTime.now());

        Long ID = 0L;
        for (Message curent : messageRepository.findAll()){
            if(curent.getId() > ID){
                ID = curent.getId();
            }
        }
        ID = ID+1L;
        mesaj_nou.setId(ID);

        m.addReplies(mesaj_nou.getId());
        messageRepository.update(m);
        messageRepository.save(mesaj_nou);
        return mesaj_nou;
    }

    public List<Long> getMessageReplies(Long id_mess) {
        if(messageRepository.findOne(id_mess) == null)
        {
            throw new ValidationException("Invalid ID message!");
        }
        Message m = messageRepository.findOne(id_mess);
        List<Long> list = new ArrayList<>();
        for(Long reply : m.getReply())
        {
            list.add(reply);
        }
        return list;
    }

    public boolean apartineTO(Message m, Utilizator u)
    {
        for(Utilizator curent : m.getTo())
        {
            if(u.getId()==curent.getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Functie care returneaza o conversatie intre doi useri, cronologic
     * @param id_1 - user 1
     * @param id_2 - user 2
     * @return List</Message>
     */
    public List<Message> getCronologicConv(Long id_1, Long id_2) {
        if(repo.findOne(id_1) == null)
        {
            throw new ValidationException("Invalid the first user!");
        }
        if(repo.findOne(id_2) == null)
        {
            throw new ValidationException("Invalid the second user!");
        }
        Utilizator u1 = repo.findOne(id_1);
        Utilizator u2 = repo.findOne(id_2);
        List<Message> conversatie = new ArrayList<>();
        for(Message m : messageRepository.findAll())
        {
            if(apartineTO(m,u2) && m.getFrom().getId()==u1.getId() || apartineTO(m,u1) && m.getFrom().getId()== u2.getId())//daca unul dintre ei este in from si celalalt in to
            {
                conversatie.add(m);
            }
        }
        //conversatie - List<Message> toate mesajele dintre 2 useri
        conversatie.sort((x, y)->compare(x, y));
        return conversatie;
    }
    public int compare(Message a, Message b) {
        return a.getData().compareTo(b.getData());

    }

    public List<Message> getMessages(Utilizator u)
    {
        List<Message> messages = new ArrayList<>();
        for(Message m : messageRepository.findAll())
        {
            if(m.getFrom().getId() == u.getId() || apartineTO(m, u))
            {
                messages.add(m);
            }
        }
        return messages;
    }

    private List<Observer> observers = new ArrayList<Observer>();

    @Override
    public void addObserver(Observer e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(x -> x.update(t));
    }

    /**
     * Returneaza lista cu utilizatorii carora le poate raspunde la un mesaj dat. (Cu exceptia sa)
     * @param message_id = mesajul la care trebuie sa se faca reply
     * @param logged = cine face reply, pentru a-l putea exclude din lista
     * @return
     */
    public List<Utilizator> getRepliers(Long message_id, Long logged) {
        Message m = messageRepository.findOne(message_id);
        List<Utilizator> repliers = new ArrayList<>();
        m.getTo().stream().filter(u->u.getId()!=logged).forEach(repliers::add);
        if(m.getFrom().getId()!=logged)
            repliers.add(m.getFrom());
        return repliers;
    }
}
