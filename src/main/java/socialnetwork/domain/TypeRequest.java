package socialnetwork.domain;

public enum TypeRequest {
    pending, approved, rejected, canceled;
}
