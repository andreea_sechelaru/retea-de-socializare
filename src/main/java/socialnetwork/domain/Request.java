package socialnetwork.domain;

import socialnetwork.domain.validators.ValidationException;

import java.time.LocalDateTime;

public class Request extends Entity<Long> {

    private Long from;
    private Long to;
    TypeRequest status;
    private LocalDateTime date;


    public Request(Long from, Long to, TypeRequest status, LocalDateTime date) {
        this.from = from;
        this.to = to;
        this.status = status;
        this.date = date;
    }

    public Request(Long from, Long to) {
        this.from = from;
        this.to = to;
        this.status = TypeRequest.pending;

    }

    public TypeRequest getStatus() {
        return status;
    }

    public void setStatus(TypeRequest status) {
        this.status = status;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Request{" +
                "from=" + from +
                ", to=" + to +
                ", status=" + status +
                ", date=" + date +
                '}';
    }

}
