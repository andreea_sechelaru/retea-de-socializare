package socialnetwork.domain;

import java.util.*;

public class Retea {

    private static Retea retea = null;

    private Retea(){}

    public static Retea getInstanceRetea()
    {
        if(retea==null)
            retea = new Retea();
        return retea;
    }

    /**
     * Se creeaza comunitatea de utilizatori drept graf unde utilizatorii sunt noduri si prieteniile sunt muchii
     * @param users - Iterable Utilizator>
     * @return - Map<Long,List Utilizator>>
     */
    private Map<Long, List<Utilizator>> createGraph(Iterable<Utilizator> users)
    {
        Map<Long,List<Utilizator>> graf = new HashMap<>();
        users.forEach((x)->graf.put(x.getId(),x.getFriends()));
        return graf;
    }

    /**
     * Parcurge in adancime UN graf - DFS
     * @param v - Long - id-ul unui Utilizator
     * @param graf - Map Long,List Utilizator>> - Graful
     * @param visited - Set Long> - vector de vizitati
     * @param rez - List Long> - componenta conexa
     */
    private void parcurgere_DFS(Long v, Map<Long,List<Utilizator>> graf, Set<Long> visited, List<Long> rez)
    {
        visited.add(v);
        rez.add(v);
        graf.get(v).forEach((x)->{if(!visited.contains(x.getId()))
            parcurgere_DFS(x.getId(),graf,visited,rez);
        });

    }

    /**
     * Calculeaza componentele conexe intr-un graf
     * @param users - Iterable Utilizator - graful cu utilizatori
     * @return - List List Long - componentele conexe
     */
    public List<List<Long>> getComponenteConexe(Iterable<Utilizator> users)
    {
        Set<Long> visited = new HashSet<>();
        int nr=0;
        List<List<Long>> componente = new ArrayList<>();
        Map<Long,List<Utilizator>> graf = this.createGraph(users);
        for(Map.Entry<Long,List<Utilizator>> pereche : graf.entrySet())
        {
            if(!visited.contains(pereche.getKey()))
            {List<Long> rez = new ArrayList<>();
                nr++;
                parcurgere_DFS(pereche.getKey(),graf,visited,rez);
                componente.add(rez);
            }
        }

        return componente;
    }

    /**
     * Numarul componentelor conexe
     * @param users - Iterable Utilizator - graful cu utilizatori
     * @return - Integer - Numarul componentelor conexe
     */
    public int NrComponenteConexe(Iterable<Utilizator> users)
    {
        List<List<Long>> componente = getComponenteConexe(users);
        return componente.size();
    }

    /**
     * Componenta conexa cu cel mai lung drum
     * @param users - Iterable Utilizator - graful cu utilizatori
     * @return - List Long - Lista de id-uri ale utilizatorilor ce reprezinta componenta conexa
     */
    public List<Long> comunitate (Iterable<Utilizator> users)
    {
        List<List<Long>> componente = getComponenteConexe(users);
        List<Long> sociabila = new ArrayList<>();
        int nr_max = Integer.MIN_VALUE;
        for(List<Long> comp : componente)
        {
            int nr = comp.size();
            if(nr > nr_max)
            {
                nr_max = nr;
                sociabila = comp;
            }
        }
        return sociabila;
    }

    public Iterable<Utilizator> longest(Iterable<Utilizator> users)
    {
        Set<Long> visited = new HashSet<>();
        //int nr=0;
        List<List<Long>> componente = new ArrayList<>();
        Map<Long,List<Utilizator>> graf = this.createGraph(users);
        List<Long> sociabila = new ArrayList<>();
        for(Map.Entry<Long,List<Utilizator>> pereche : graf.entrySet())
        {
            if(!visited.contains(pereche.getKey()))
            {List<Long> rez = new ArrayList<>();
                //nr++;
                parcurgere_DFS(pereche.getKey(),graf,visited,rez);
                componente.add(rez);
            }
        }
        return null;
    }




}

