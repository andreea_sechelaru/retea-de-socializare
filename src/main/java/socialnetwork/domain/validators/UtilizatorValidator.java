package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;


import java.util.ArrayList;
import java.util.List;

public class UtilizatorValidator implements Validator<Utilizator> {
    @Override
    /**
     * Valideaza o entitate de tip Utilizator
     * id - int ,id ul Utiliatorului, id>0
     * first name , first name ul Utiliatorului - string , first name != ''
     * last name , last name ul Utiliatorului - string , last name != ''
     */
    public void validate(Utilizator entity) throws ValidationException {
        List<String> msgs = new ArrayList<String>();
        if(entity.getId() < 0) msgs.add("Id should be positive!");
        if(entity.getFirstName().length() == 0) msgs.add("First name couldn't be vid!");
        if(entity.getLastName().length() == 0) msgs.add("Last name couldn't be vid!");
        if (msgs.size() > 0)
        {
            throw new ValidationException(msgs);
        }
    }
}
