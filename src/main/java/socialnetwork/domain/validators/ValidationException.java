package socialnetwork.domain.validators;

import java.util.List;

public class ValidationException extends RuntimeException {
    public ValidationException() {
    }

    public ValidationException(List<String> message) {
        super(String.valueOf(message));
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }

    public ValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
