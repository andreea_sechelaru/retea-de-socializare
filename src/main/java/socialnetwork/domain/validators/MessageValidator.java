package socialnetwork.domain.validators;

import socialnetwork.domain.Message;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;

import java.util.ArrayList;
import java.util.List;

public class MessageValidator implements Validator<Message> {
    @Override
    public void validate(Message entity) throws ValidationException {
        List<String> msgs = new ArrayList<String>();
        if(entity.getId()< 0) msgs.add("Users are invalid!");
        if(entity.getMessage().length() == 0) msgs.add("Message can't be vid!");
        if (msgs.size() > 0)
        {
            throw new ValidationException(msgs);
        }
    }
}
