package socialnetwork.domain.validators;

import socialnetwork.domain.Prietenie;

import java.util.ArrayList;
import java.util.List;

public class FriendshipValidator implements Validator<Prietenie> {
    @Override
    /**
     * Valideaza o entitate de tip prietenie
     * id - int , id-urile celor doi utilizatori, trebuie sa existe
     * is- urile trebuie sa fie diferite
     */
    public void validate(Prietenie entity) throws ValidationException {
        List<String> msgs = new ArrayList<String>();
        if(entity.getId().getLeft() < 0 || entity.getId().getRight() < 0) msgs.add("Users are invalid!");
        if(entity.getId().getLeft() == entity.getId().getRight()) msgs.add("Users must not be equals!");

        if (msgs.size() > 0)
        {
            throw new ValidationException(msgs);
        }
    }
}
