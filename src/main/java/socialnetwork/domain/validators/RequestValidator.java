package socialnetwork.domain.validators;

import socialnetwork.domain.Message;
import socialnetwork.domain.Request;

import java.util.ArrayList;
import java.util.List;

public class RequestValidator implements Validator<Request>{
    @Override
    public void validate(Request entity) throws ValidationException {
        List<String> msgs = new ArrayList<String>();
        if(entity.getFrom() < 0 || entity.getTo() < 0) msgs.add("Users are invalid!");
        if(entity.getFrom() == entity.getTo()) msgs.add("Users must not be equals!");

        if (msgs.size() > 0)
        {
            throw new ValidationException(msgs);
        }
    }
}
