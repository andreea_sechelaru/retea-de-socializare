package socialnetwork.domain.validators;

public class RepoException extends RuntimeException{
    public RepoException(String message) {
        super(message);
    }

    public RepoException(Throwable cause) {
        super(cause);
    }
}
