package socialnetwork.domain;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Utilizator extends Entity<Long>{
    private String firstName;
    private String lastName;
    private List<Utilizator> friends;

    public Utilizator(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.friends = new ArrayList<Utilizator>();
    }


    /**
     * Prenumele Utilizatorului
     * @return - String
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Seteaza prenumele utilizatorului
     * @param firstName - String
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Numele utilizatorului
     * @return - String
     */
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Adauga un prieten in lista de prieteni a Utilizatorului u
     * @param u - Utilizator
     */
    public void addPrieten(Utilizator u) {
        friends.add(u);

    }

    /**
     * Sterge un prieten in lista de prieteni a Utilizatorului u
     * @param u - Utilizator
     */
    public void removePrieten(Utilizator u){
        friends.remove(u);
    }


    public List<Utilizator> getFriends() {
        return friends;
    }



    @Override
    public String toString() {
        return "Utilizator{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilizator)) return false;
        Utilizator that = (Utilizator) o;
        return getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName()) &&
                getFriends().equals(that.getFriends());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName());
    }

    public String getIdString() {
        return getId().toString();
    }

}

