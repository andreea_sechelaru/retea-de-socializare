package socialnetwork.domain;

import java.time.LocalDateTime;


public class Prietenie extends Entity<Tuple<Long,Long>> {

    LocalDateTime date;

    public Prietenie(LocalDateTime now) {
        date=now;
    }


    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public String toString() {
        Prietenie p;
        return "Prietenie{" + "u1 = " + getId().getLeft() + ", u2 = " + getId().getRight() +
                ", date=" + date.toLocalDate() +
                '}';
    }
}
