package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Message extends Entity<Long> {

    private Utilizator from;
    private List<Utilizator> to;
    private String message;
    private LocalDateTime data;
    private List<Long> reply = new ArrayList<Long>();

    public Message(Utilizator from, List<Utilizator> to, String message, LocalDateTime data) {

        this.from = from;
        this.to = to;
        this.message = message;
        this.data = data;
        this.reply = new ArrayList<Long>();
    }

    public Message() {
    }

    public Utilizator getFrom() {
        return from;
    }

    public List<Utilizator> getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getData() {
        return data;
    }

    public List<Long> getReply() {
        return reply;
    }



    public void setFrom(Utilizator from) {
        this.from = from;
    }

    public void setTo(List<Utilizator> to) {
        this.to = to;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public void setReply(List<Long> reply) {
        this.reply = reply;
    }

    public void addReplies(Long m)
    {
        this.reply.add(m);
    }

    @Override
    public String toString() {
        return "Message{" +
                "from=" + from +
                ", to=" + to +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", reply=" + reply +
                '}';
    }

    public String getStringFrom() {
        return from.getFirstName() + " " + from.getLastName();
    }

    public String getStringTo() {
        StringBuilder stringTo = new StringBuilder();
        for (Utilizator u : to)
            stringTo.append(u.getFirstName() + " " + u.getLastName() + "\n");
        return stringTo.toString();
    }
}
