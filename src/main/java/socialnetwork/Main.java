package socialnetwork;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.FriendshipValidator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.FriendshipFile;
import socialnetwork.repository.file.MessageFile;
import socialnetwork.domain.validators.MessageValidator;
import socialnetwork.repository.file.RequestFile;
import socialnetwork.repository.file.UtilizatorFile;
import socialnetwork.service.MessageService;
import socialnetwork.service.RequestService;
import socialnetwork.service.Service;
import socialnetwork.ui.Console;
import socialnetwork.domain.validators.RequestValidator;


public class Main{
    public static void main(String[] args){
        //String fileName=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users");
        String fileName="data/users.csv";
        String fileName2="data/friendships.csv";
        String fileName3="data/messages.csv";
        String fileName4="data/request.csv";

        Repository<Long,Utilizator> userFileRepository = new UtilizatorFile(fileName, new UtilizatorValidator());
        Repository<Tuple<Long, Long>, Prietenie>  friendshipRepository = new FriendshipFile(fileName2, new FriendshipValidator());
        Repository<Long, Message> messageRepository = new MessageFile(fileName3, new MessageValidator());
        Repository<Long, Request> requestRepository = new RequestFile(fileName4, new RequestValidator());

        userFileRepository.findAll().forEach(System.out::println);
        friendshipRepository.findAll().forEach(System.out::println);
        messageRepository.findAll().forEach(System.out::println);
        requestRepository.findAll().forEach(System.out::println);

        Service srv = new Service(userFileRepository, friendshipRepository);
        MessageService msgsrv = new MessageService(userFileRepository, messageRepository);
        RequestService rqsrv = new RequestService(requestRepository, userFileRepository, friendshipRepository, srv);

        Console ui = new Console(srv, msgsrv, rqsrv);

        try {
            ui.start();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}


