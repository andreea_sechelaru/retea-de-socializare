package socialnetwork.repository.file;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Request;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.TypeRequest;
import socialnetwork.domain.validators.Validator;

import java.time.LocalDateTime;
import java.util.List;

public class RequestFile extends AbstractFileRepository<Long, Request>{

    public RequestFile(String fileName, Validator<Request> validator) {
        super(fileName, validator);
    }

    @Override
    public Request extractEntity(List<String> attributes) {
        Long from = Long.parseLong(attributes.get(1));
        Long to = Long.parseLong(attributes.get(2));
        LocalDateTime date = LocalDateTime.parse(attributes.get(4));
        Request r = new Request(from, to, TypeRequest.valueOf(attributes.get(3)), date);
        r.setId(Long.parseLong(attributes.get(0)));
        return r;

    }

    @Override
    protected String createEntityAsString(Request entity) {
        return entity.getId() +";"+entity.getFrom() + ";" + entity.getTo() + ";" + entity.getStatus().toString()+ ";" + entity.getDate();
    }
}
