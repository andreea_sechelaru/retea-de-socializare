package socialnetwork.repository.file;

import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.service.MessageService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MessageFile extends AbstractFileRepository<Long, Message>{


    public MessageFile(String fileName, Validator<Message> validator) {
        super(fileName, validator);

    }

    @Override
    public Message extractEntity(List<String> attributes) {
        String[] from = attributes.get(1).split("-");
        Utilizator u = new Utilizator(from[1], from[2]);
        u.setId(Long.parseLong(from[0]));
        String[] to = attributes.get(2).split(",");
        List<Utilizator> to_final = new ArrayList<>();
        for (String el : to) {
            String[] user = el.split("-");
            Utilizator u_curent = new Utilizator(user[1], user[2]);
            u_curent.setId(Long.parseLong(user[0]));
            to_final.add(u_curent);
        }
        String message = attributes.get(3);
        LocalDateTime data = LocalDateTime.parse((CharSequence) attributes.get(4));
        List<Long> replies = new ArrayList<>();
        Message m = new Message(u, to_final, message, data);
        if(attributes.get(5).length() == 0){
            m.setReply(replies);
        }
        else{
            try{
                String[] reply = attributes.get(5).split(",");
                for(String el : reply){
                    Long m_curent = Long.parseLong(el);
                    replies.add(m_curent);
                }
                m.setReply(replies);
            }
            catch(ArrayIndexOutOfBoundsException ignored){};
        }


        m.setId(Long.parseLong((String) attributes.get(0)));
        return m;

    }

    @Override
    protected String createEntityAsString(Message entity) {
        String lista = entity.getTo().stream().map(x->x.getId()+"-"+x.getFirstName()+"-"+x.getLastName()).reduce("", (x1, x2)->x1+","+x2);
        lista = lista.substring(1, lista.length());
        if(entity.getReply().size() == 0){
            return entity.getId()+";"+ entity.getFrom().getId() + "-" + entity.getFrom().getFirstName() + "-" + entity.getFrom().getLastName() + ";"+ lista +";"+entity.getMessage()+";"+entity.getData() + ";"  + ";";
        }
        else{
            String replies = entity.getReply().stream().map(x->x.toString()).reduce("", (x1,x2)->x1+","+x2);
            if(replies.length() > 0)
                replies = replies.substring(1, replies.length());
            return entity.getId()+";"+ entity.getFrom().getId() + "-" + entity.getFrom().getFirstName() + "-" + entity.getFrom().getLastName() + ";"+ lista +";"+entity.getMessage()+";"+entity.getData() + ";" + replies + ";";
        }

    }
}
