package socialnetwork.repository.file;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.time.LocalDateTime;
import java.util.List;

public class FriendshipFile extends  AbstractFileRepository<Tuple<Long, Long>, Prietenie> {

    public FriendshipFile(String fileName, Validator validator) {
        super(fileName, validator);
    }

    @Override
    public Prietenie extractEntity(List<String> attributes) {
        Prietenie p = new Prietenie(LocalDateTime.parse((CharSequence) attributes.get(2)));
        Tuple prietenieId = new Tuple(Long.parseLong((String) attributes.get(0)), Long.parseLong((String) attributes.get(1)));
        p.setId(prietenieId);

        return p;
    }

    @Override
    protected String createEntityAsString(Prietenie entity) {
        return entity.getId().getLeft()+";"+entity.getId().getRight()+";"+entity.getDate();
    }
}
