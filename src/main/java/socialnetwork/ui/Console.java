package socialnetwork.ui;

import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Request;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.RepoException;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.MessageService;
import socialnetwork.service.RequestService;
import socialnetwork.service.Service;

//import java.security.Provider;
import java.time.Month;
import java.util.*;

public class Console {

    private Service srv;
    private RequestService rqsrv;
    private MessageService msgsrv;


    public Console(Service srv, MessageService msgsrv, RequestService rqsrv) {
        this.srv = srv;
        this.msgsrv = msgsrv;
        this.rqsrv = rqsrv;
    }

    /**
     * Adauga un utilizator citit de la tastatura (Id, Nume, Prenume)
     */
    private void addUtilizator()
    {
        String FirstName, LastName;
        Scanner reader = new Scanner(System.in);
        Scanner reader1 = new Scanner(System.in);
        System.out.println("Enter ID: ");
        Long ID = reader1.nextLong();
        System.out.println("Enter First Name: ");
        FirstName = reader.nextLine();
        System.out.println("Enter Last Name: ");
        LastName = reader.nextLine();

        Utilizator u = new Utilizator(FirstName, LastName);
        u.setId(ID);

        try{
            srv.addUtilizator(u);
        }catch (RepoException e)
        {
            System.out.println(e.getMessage());
        }
        catch (ValidationException e)
        {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Sterge un utilizator citind de la tastatura id-ul lui
     */
    private void removeUtilizator() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter ID: ");
        Long ID = reader.nextLong();
        try {
            srv.removeUtilizator(ID);
        } catch (RepoException e){
            System.out.println(e.getMessage());
        }
        catch (ValidationException e)
        {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Adauga o prietenie dintre doi utilizatori citind de la tastatura id-urile lor
     */
    private void addPrieten()
    {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter the first ID: ");
        Long ID = reader.nextLong();
        System.out.println("Enter the second ID: ");
        Long ID2 = reader.nextLong();
        try {
            srv.addPrietenie(ID, ID2);
        }
        catch (RepoException | ValidationException e)
        {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Sterge o prietenie dintre doi utilizatori citind de la tastatura id-urile lor
     */
    private void removePrieten()
    {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter the first ID: ");
        Long ID = reader.nextLong();
        System.out.println("Enter the second ID: ");
        Long ID2 = reader.nextLong();
        try {
            srv.removePrieten(ID, ID2);
        }catch (RepoException | ValidationException e)
        {
            System.out.println(e.getMessage());
        }

    }

    private void findfriends()
    {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter the ID: ");
        Long ID = reader.nextLong();
        try{
            Utilizator u = srv.gaseste(ID);
            List<Prietenie> friends = srv.getPrietenii();
            friends.stream()
                    .filter(x->x.getId().getLeft() == ID)
                    .forEach(x->System.out.println("Nume: " + srv.gaseste(x.getId().getRight()).getLastName() + " Prenume: " + srv.gaseste(x.getId().getRight()).getFirstName() + " Data: " + x.getDate()));
            friends.stream()
                    .filter(x->x.getId().getRight() == ID)
                    .forEach(x->System.out.println("Nume: " + srv.gaseste(x.getId().getLeft()).getLastName() + " Prenume: " + srv.gaseste(x.getId().getLeft()).getFirstName() + " Data: " + x.getDate()));

        }
        catch (ValidationException e)
        {
            System.out.println(e.getMessage());
        }

    }

    private void findfriendsDate()
    {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter the ID: ");
        Long ID = reader.nextLong();
        try{
            Utilizator u = srv.gaseste(ID);
            System.out.println("Enter the Date: ");
            try {
                Month date = Month.valueOf(reader.next());
                List<Prietenie> friends = srv.getPrietenii();
                friends.stream()
                        .filter(x -> x.getId().getLeft() == ID && x.getDate().getMonth() == date)
                        .forEach(x -> System.out.println("Nume: " + srv.gaseste(x.getId().getRight()).getLastName() + " Prenume: " + srv.gaseste(x.getId().getRight()).getFirstName() + " Data: " + x.getDate()));
                friends.stream()
                        .filter(x -> x.getId().getRight() == ID && x.getDate().getMonth() == date)
                        .forEach(x -> System.out.println("Nume: " + srv.gaseste(x.getId().getLeft()).getLastName() + " Prenume: " + srv.gaseste(x.getId().getLeft()).getFirstName() + " Data: " + x.getDate()));

            }
            catch (IllegalArgumentException e)
            {
                System.out.println("Invalid month!!");
            }
        }
        catch (ValidationException e)
        {
            System.out.println(e.getMessage());
        }


    }

    private void sendMessage() {
        String message;
        Scanner reader1 = new Scanner(System.in);
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter the ID from: ");
        Long ID = reader.nextLong();
        System.out.println("How many users? ");
        Long len = reader.nextLong();
        System.out.println("Enter the IDs to: ");
        List<Long> TO = new ArrayList<Long>();
        int i = 0;
        while (i < len){
            Long ID1 = reader.nextLong();
            TO.add(ID1);
            i++;
        }
        System.out.println("Enter the message: ");
        message = reader1.nextLine();
        try {
            Message m = msgsrv.messageToOne(ID, TO, message);
            System.out.println("S-a trimis mesajul! ");
            System.out.println("ID: " + m.getId()+" From: "+m.getFrom().getId() + " To: " + m.getTo().size() + "user(s)");
        }
        catch(ValidationException e){
            System.out.println(e.getMessage());
        }
    }

    public void replyMessage()
    {
        Scanner reader1 = new Scanner(System.in);
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter message id:");
        Long id_mess = reader.nextLong();
        System.out.println("Enter the ID from: ");
        Long ID = reader.nextLong();
        System.out.println("Do you want to reply to all users? Yes/No");
        String answer = reader1.nextLine();
        switch (answer) {
            case "Yes":
                System.out.println("Enter the message: ");
                String message = reader1.nextLine();
                try {
                    Message m = msgsrv.replyToAll(id_mess, ID, message);
                    System.out.println("Reply ID: " + m.getId() + " From: " + m.getFrom().getId() + " To: " + m.getTo().size() + " user(s)");
                } catch (ValidationException e) {
                    System.out.println(e.getMessage());
                }
                return;
            case "No":
                System.out.println("Give the specified user id:");
                Long id_user = reader.nextLong();
                System.out.println("Enter the message: ");
                String message1 = reader1.nextLine();
                try {
                    Message m = msgsrv.replyToOne(id_mess, ID, id_user, message1);
                    System.out.println("Reply ID: " + m.getId() + " From: " + m.getFrom().getId() + " To: " + m.getTo().size() + " user(s)");
                } catch (ValidationException e) {
                    System.out.println(e.getMessage());
                }
                return;
            }

    }
    private void getMessageReplies() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter message id:");
        Long id_mess = reader.nextLong();
        try{
            List<Long> list = msgsrv.getMessageReplies(id_mess);
            if(list.size() == 0)
                System.out.println("There s no replies to this message");
            System.out.println("All replies' id: " + list);
        }
        catch (ValidationException e)
        {
            System.out.println(e.getMessage());
        }
    }

    private void getConversation() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter the first id:");
        Long id_1 = reader.nextLong();
        System.out.println("Enter the second id:");
        Long id_2 = reader.nextLong();
        try {
            List<Message> mesaje = msgsrv.getCronologicConv(id_1, id_2);
            if(mesaje.size()==0)
                System.out.println("There's no message between this two users");
            for (Message m : mesaje) {
                System.out.println(m.getId() + ": " + m.getMessage());
            }
            System.out.println();
        }
        catch (ValidationException e)
        {
            System.out.println(e.getMessage());
        }

    }

    private void sendFriendshipRequest() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter the first id:");
        Long id_1 = reader.nextLong();
        System.out.println("Enter the second id:");
        Long id_2 = reader.nextLong();
        try{
            rqsrv.sendRequest(id_1, id_2);
            System.out.println("Request sent");
        }catch(ValidationException e)
        {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Accepta o cerere de prietenie
     */
    private void approveFriendshipRequest() {
        Scanner reader = new Scanner(System.in);
        Scanner reader1 = new Scanner(System.in);
        System.out.println("Enter the first id:");
        Long id_1 = reader.nextLong();
        System.out.println("Enter the second id:");
        Long id_2 = reader.nextLong();
        System.out.println("Enter the status (approve/reject): ");
        String status = reader1.nextLine();
        try {
            rqsrv.approve(id_1, id_2);
            System.out.println("Request processed");
        }catch(ValidationException e)
        {
            System.out.println(e.getMessage());
        }


    }

    private void showFriendRequests(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter the id:");
        Long id = reader.nextLong();
        List<Request> requests = rqsrv.showFriendRequests(id);
        for(Request request : requests){
            System.out.println(request);
        }
    }

    private void find()
    {
        List<Utilizator> users = srv.findfriends("Andreea", "Sechelaru");
        for(Utilizator user : users)
        {
            System.out.println(user.getFirstName() + " " + user.getLastName());
        }
    }

    private void cancelRequest(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter the first id:");
        Long id1 = reader.nextLong();
        System.out.println("Enter the second id:");
        Long id2 = reader.nextLong();
        try {
            rqsrv.cancelRequest(id1, id2);
            System.out.println("Request canceled");
        }catch(ValidationException e)
        {
            System.out.println(e.getMessage());
        }

    }

    public void start() throws Exception
    {
        while(true)
        {
            System.out.println(" 1. Add Utilizator");
            System.out.println(" 2. Remove Utilizator");
            System.out.println(" 3. Add prieten");
            System.out.println(" 4. Remove prieten");
            System.out.println(" 5. Numar de comunitati");
            System.out.println(" 6. Comunitatea cu cei mai multi useri");
            System.out.println(" 7. Comunitatea cea mai sociabila");
            System.out.println(" 8. User's friends");
            System.out.println(" 9. User's friends in a month");
            System.out.println("10. Send a message");
            System.out.println("11. Reply a message");
            System.out.println("12. All replies to a given message");
            System.out.println("13. Get conversations");
            System.out.println("14. Send friendship request");
            System.out.println("15. Approve request");
            System.out.println("16. Show friend requests");
            System.out.println("17. User's friends by name");
            System.out.println("18. Cancel request");
            System.out.println(" 0. Exit");

            Scanner reader = new Scanner(System.in);
            int nr = reader.nextInt();
            switch (nr)
            {
                case 1:
                    addUtilizator();
                    break;
                case 2:
                    removeUtilizator();
                    break;
                case 3:
                    addPrieten();
                    break;
                case 4:
                    removePrieten();
                    break;
                case 5:
                    System.out.println("Numarul de comunitati: " + srv.componenteConexe());
                    break;
                case 6:
                    System.out.println("Cea mai sociabila comunitate: " + srv.comunitateSociabila());
                    break;
                case 7:
                    System.out.println("Cea mai sociabila comunitate: " + srv.cel_mai_lung_drum());
                    break;
                case 8:
                    findfriends();
                    break;
                case 9:
                    findfriendsDate();
                    break;
                case 10:
                    sendMessage();
                    break;
                case 11:
                    replyMessage();
                    break;
                case 12:
                    getMessageReplies();
                    break;
                case 13:
                    getConversation();
                    break;
                case 14:
                    sendFriendshipRequest();
                    break;
                case 15:
                    approveFriendshipRequest();
                    break;
                case 16:
                    showFriendRequests();
                    break;
                case 17:
                    find();
                    break;
                case 18:
                    cancelRequest();
                    break;
                case 0:
                    return;
                default:
                    System.out.println("!Comanda invalida!");
                    break;
            }
        }
    }




}
