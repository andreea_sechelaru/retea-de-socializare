package events;

import controller.FriendshipsUsersController;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;

public class FriendshipsUsersChangeEvent implements  Event{
    private ChangeEvent type;
    private Utilizator data_user, oldData_user;
    private Prietenie data_friendships, oldData_friendships;

    public FriendshipsUsersChangeEvent(ChangeEvent type, Prietenie data_friendships)
    {
        this.type = type;
        this.data_friendships = data_friendships;
    }

    public ChangeEvent getType(){return type;}

}
